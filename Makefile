.PHONY: help
.DEFAULT_GOAL = help

DOCKER_COMPOSE=@docker-compose
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec 
PHP_DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE_EXEC) exec php 
COMPOSER=$(PHP_DOCKER_COMPOSE_EXEC) php -d memory_limit=-1 /usr/local/bin/composer
SYMFONY_CONSOLE=$(PHP_DOCKER_COMPOSE_EXEC) bin/console 

## -- Docker ------------------------------------
start: ## Lancer les conteneurs docker
	$(DOCKER_COMPOSE) up -d 

stop: ## Arreter les conteurs docker
	$(DOCKER_COMPOSE) stop 

rm: stop ## Supprimer les containers docker
	$(DOCKER_COMPOSE) rm -f

restart: rm start ## Redémarrer les containers

ssh-php: ## Connextion au bash dans le conteneur
	$(PHP_DOCKER_COMPOSE_EXEC) bash 

## -- Symfony ------------------------------------
vendor-install: ## Installation des vendors
	$(COMPOSER) install

vendor-update: ## Mise à jour des vendors
	$(COMPOSER) update 

clean-vendor: cc-hard ## Suppression du répertoire vendor puis réinstall
	$(PHP_DOCKER_COMPOSE_EXEC) rm -Rf vendor 
	$(PHP_DOCKER_COMPOSE_EXEC) rm composer.lock
	$(COMPOSER) install

cc: ## Vider le cache
	$(SYMFONY_CONSOLE) c:c

cc-test: ## Supprimer le cache de l'environnement de test
	$(SYMFONY_CONSOLE) c:c --env=test

cc-hard: ## Supprimer le répertoire de cache
	$(PHP_DOCKER_COMPOSE_EXEC) rm -fR var/cache/*

clean-db: ## Réinitialiser la base de donnée
	- $(SYMFONY_CONSOLE) d:d:d --force --connection 
	$(SYMFONY_CONSOLE) d:d:c
	$(SYMFONY_CONSOLE) d:m:m --no-interaction
	$(SYMFONY_CONSOLE) d:f:l --no-interaction

clean-db-test: cc-hard cc-test ## Réinitialiser la base de donnée en environnement de test
	- $(SYMFONY_CONSOLE) d:d:d --force --env=test 
	$(SYMFONY_CONSOLE) d:d:c --env=test
	$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test
	$(SYMFONY_CONSOLE) d:f:l --no-interaction --env=test

test-unit: ## Lancement des tests unitaires
	$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit test/Unit/

test-func: clean-db-test  ## Lancement des test fonctionnels
	$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit tests/Func

test: test-func test-unit ## Lancement de tous les tests

cs: ## Lancement du PHP cs
	$(PHP_DOCKER_COMPOSE_EXEC) vendor/bin/phpcs -n 

help: ## Liste des commandes
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
