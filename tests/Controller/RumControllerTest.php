<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RumControllerTest extends WebTestCase 
{
  private $client = null;
  private $crawler;

  public function setUp(): void
  {
    $this->client = static::createClient();
    $this->crawler = $this->client->request('Get', '/rums/');
  }

  public function testPageFind() 
  {
    $this->assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testH1Present() 
  {
    $this->assertSelectorTextContains('h2','Nos Rhums arrangés');
  }

}