<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class BookingControllerTest extends WebTestCase 
{
  private $client = null;
  private $crawler;

  public function setUp(): void
  {
    $this->client = static::createClient();
    $this->crawler = $this->client->request('Get', '/booking/');
  }

  public function testPageFind() 
  {
    $this->assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testH1Present() 
  {
    $this->assertSelectorTextContains('span','Réservation');
  }

}