<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PresentationControllerTest extends WebTestCase 
{
  private $client = null;
  private $crawler;

  public function setUp(): void
  {
    $this->client = static::createClient();
    $this->crawler = $this->client->request('Get', '/');
  }

  public function testHomepageIsUp() 
  {
    $this->assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testH3Present() 
  {
    $this->assertSelectorTextContains('h3','Bienvenue');
  }

}