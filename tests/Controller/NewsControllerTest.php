<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class NewsControllerTest extends WebTestCase 
{
  private $client = null;
  private $crawler;

  public function setUp(): void
  {
    $this->client = static::createClient();
    $this->crawler = $this->client->request('Get', '/newsletter/');
  }

  public function testPageFind() 
  {
    $this->assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testHtmlPresent() 
  {
    $this->assertSame(1, $this->crawler->filter('html:contains("Newsletter")')->count());
    $this->assertSame(1, $this->crawler->filter('html:contains("Réservation")')->count());

  }

}