<?php


namespace App\Tests\NavLink;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NavbarLinkTest extends WebTestCase
{
    private $client = null;
    private $crawler;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }
    public function testHomeLink()
    {
        $this->crawler = $this->client->request('Get', '/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

        $this->crawler = $this->client->request('Get', '/carte/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

        $this->crawler = $this->client->request('Get', '/booking/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

        $this->crawler = $this->client->request('Get', '/archives/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

        $this->crawler = $this->client->request('Get', '/events/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

        $this->crawler = $this->client->request('Get', '/contact/');
        $link = $this->crawler->selectLink('Accueil')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Bienvenue", $info);

    }

    public function testMenuLink()
    {
        $this->crawler = $this->client->request('Get', '/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

        $this->crawler = $this->client->request('Get', '/carte/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

        $this->crawler = $this->client->request('Get', '/booking/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

        $this->crawler = $this->client->request('Get', '/archives/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

        $this->crawler = $this->client->request('Get', '/events/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

        $this->crawler = $this->client->request('Get', '/contact/');
        $link = $this->crawler->selectLink('Cartes')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h3')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Notre carte", $info);

    }

    public function testBookingLink()
    {
        $this->crawler = $this->client->request('Get', '/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);

        $this->crawler = $this->client->request('Get', '/carte/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);

        $this->crawler = $this->client->request('Get', '/booking/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);

        $this->crawler = $this->client->request('Get', '/archives/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);

        $this->crawler = $this->client->request('Get', '/events/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);

        $this->crawler = $this->client->request('Get', '/contact/');
        $link = $this->crawler->selectLink('Réservation')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('span')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Réservation", $info);



    }

    public function testArchivesLink()
    {
        $this->crawler = $this->client->request('Get', '/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

        $this->crawler = $this->client->request('Get', '/carte/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

        $this->crawler = $this->client->request('Get', '/booking/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

        $this->crawler = $this->client->request('Get', '/archives/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

        $this->crawler = $this->client->request('Get', '/events/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

        $this->crawler = $this->client->request('Get', '/contact/');
        $link = $this->crawler->selectLink('Archives')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Gallerie", $info);

    }

    public function testContactLink()
    {
        $this->crawler = $this->client->request('Get', '/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

        $this->crawler = $this->client->request('Get', '/carte/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

        $this->crawler = $this->client->request('Get', '/booking/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

        $this->crawler = $this->client->request('Get', '/archives/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

        $this->crawler = $this->client->request('Get', '/events/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

        $this->crawler = $this->client->request('Get', '/contact/');
        $link = $this->crawler->selectLink('Contact')->link();
        $this->crawler = $this->client->click($link);
        $info = $this->crawler->filter('h2')->text();
        $info = $string = trim(preg_replace('/\s\s+/', ' ', $info));
        $this->assertSame("Contact", $info);

    }
}