<?php 

namespace App\DataFixtures;

use App\Entity\Cocktail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CocktailFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 10; $i++)
      {
        $cocktail = new Cocktail();
        $cocktail->setName("Cocktail$i")
              ->setPrice($i+3)
              ->setPriceHH($i)
              ->setQuantity("20 cl") 
              ;
        $ingredients = [];
        for($j=1; $j < random_int(3,5); $j++)
        {
          array_push($ingredients,"ingredients$j");
        }
        $cocktail->setIngredients($ingredients);

        $manager->persist($cocktail);
      }      
        $manager->flush();
    }
}