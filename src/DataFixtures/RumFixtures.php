<?php 

namespace App\DataFixtures;

use App\Entity\Rum;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class RumFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $rum = new Rum();
        $rum->setName("Rum$i")
              ->setPrice($i)
              ->setDescription("La description du Rhum$i")
              ->setQuantity("12 cl") 
              ;
        $manager->persist($rum);
      }      
      $manager->flush();
    }
}