<?php 

namespace App\DataFixtures;

use App\Entity\Alcools;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AlcoolFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $alcool = new Alcools();
        $alcool->setName("L'alcool $i")
              ->setPrice($i+2)
              ->setQuantity("5 cl")
              ;
        $manager->persist($alcool);
      }      
        $manager->flush();
    }
}