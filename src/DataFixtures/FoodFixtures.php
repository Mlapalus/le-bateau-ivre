<?php 

namespace App\DataFixtures;

use App\Entity\Food;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class FoodFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       for ($i=0; $i < 5; $i++)
      {
        $food = new Food();
        $food->setName("Food$i")
              ->setPrice($i)
              ->setDescription("La description du Food$i")
              ->setQuantity("grande") 
              ;
        $manager->persist($food);
      }      

        $manager->flush();
    }
}