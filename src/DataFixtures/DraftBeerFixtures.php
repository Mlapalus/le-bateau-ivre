<?php 

namespace App\DataFixtures;

use App\Entity\DraftBeer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DraftBeerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $draft = new DraftBeer();
        $draft->setName("Bière Pression $i")
              ->setPrice($i+2)
              ->setDescription("La description de la bière $i")
              ->setQuantity("25 cl")
              ->setPriceHH($i) 
              ;
        $manager->persist($draft);
      }      
        $manager->flush();
    }
}