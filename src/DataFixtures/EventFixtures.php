<?php 

namespace App\DataFixtures;

use App\Entity\Event;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class EventFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $event = new Event();
        $event->setName("Event$i")
              ->setDescription("La description de l'evenement$i")
              ;
        $manager->persist($event);
      }      
        $manager->flush();
    }
}