<?php 

namespace App\DataFixtures;

use App\Entity\Shot;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ShotFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $shot = new Shot();
        $shot->setName("Shot$i")
              ->setPrice($i)
              ->setDescription("La description du shot$i")
              ->setQuantity("2 cl") 
              ;
        $manager->persist($shot);
      }      
      $manager->flush();
    }
}