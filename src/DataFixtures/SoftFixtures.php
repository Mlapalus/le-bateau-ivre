<?php 

namespace App\DataFixtures;

use App\Entity\Soft;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class SoftFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $soft = new Soft();
        $soft->setName("Soft$i")
            ->setPrice($i)
            ->setQuantity('20cl')
            ->setDescrtiption("La description du soft$i")
              ;
        $manager->persist($soft);
  }
      $manager->flush();
    }
}