<?php 

namespace App\DataFixtures;

use App\Entity\Photo;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class PhotoFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $photo = new Photo();
        $photo->setName("Photo$i")
              ->setImage("https://via.placeholder.com/150")
              ;
        $manager->persist($photo);
      }      
      $manager->flush();
    }
}