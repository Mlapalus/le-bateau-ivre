<?php 

namespace App\DataFixtures;

use App\Entity\Wine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class WineFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      for ($i=0; $i < 5; $i++)
      {
        $wine = new Wine();
        $wine->setName("Wine$i")
              ->setPrice($i)
              ->setDescription("La description du vin$i")
              ->setQuantity("12 cl") 
              ;
        $manager->persist($wine);
      }      
      $manager->flush();
    }
}