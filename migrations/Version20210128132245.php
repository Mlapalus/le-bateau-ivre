<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210128132245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE alcools DROP image');
        $this->addSql('ALTER TABLE cocktail DROP image');
        $this->addSql('ALTER TABLE draft_beer DROP image');
        $this->addSql('ALTER TABLE event DROP image');
        $this->addSql('ALTER TABLE food DROP image');
        $this->addSql('ALTER TABLE rum DROP image');
        $this->addSql('ALTER TABLE shot DROP image');
        $this->addSql('ALTER TABLE soft DROP image');
        $this->addSql('ALTER TABLE wine DROP image');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE alcools ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cocktail ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE draft_beer ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE food ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE rum ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE shot ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE soft ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE wine ADD image VARCHAR(255) DEFAULT NULL');
    }
}
